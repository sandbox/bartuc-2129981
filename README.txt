
--------------------------------------------------------------------------------
                    STAC (Simple Taxonomy Access Control)
--------------------------------------------------------------------------------

Maintainer: Bryan Jones (Bartuc)

The STAC (Simple Taxonomy Access Control) modules allows you add simple taxonomy
control to nodes using what Drupal already gives you: Fields and Taxonomy.
Simply choose a vocabulary, a field, and set the permissions.

Project: http://drupal.org/project/stac

Installation
------------
Drush is the easiest way to install and enable the module.
  drush dl stac
  drush en stac

If you dont have Drush, do the following.
  Download the module from http://drupal.org/project/stac.
  Place the module in your site's module directory (usually sites/all/modules/)
  Enable the module through the Drupal interface.

Configuration
-------------
Once installed, navigate to the UI at admin/config/content/stac and choose a
vocabulary and a field. Permissions can then be set at admin/people/permissions.

How to Use
----------
Many sites need to show certain content for certain user roles. Without a module
there is no easy way to accomplish this seemingly simple task. With STAC all you
have to do is the following:

Add a vocabulary with terms along the lines of:
  - Everyone
  - Visitors
  - Members
  - etc...

Attach that vocabulary to a content type by adding a term reference. For example
navigate to the page content type, manage fields, add term reference and select
the vocabulary you just created.

Navigate to the UI at admin/config/content/stac and match the vocabulary with
the field.

Go to the permissions table at admin/people/permissions and set your roles for
each term.

Done.
